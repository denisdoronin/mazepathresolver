var Maze = {
    /**
     * 0 - no border
     * 1 - only right border
     * 2 - only bottom border
     * 3 - both right and bottom borders
     */
    data: [
        [0, 2, 0, 0, 0, 2, 1, 1],
        [1, 2, 1, 3, 1, 1, 0 ,3],
        [1, 1, 3, 1, 1, 1, 3, 1],
        [1, 1, 0 ,3, 3, 0, 0, 1],
        [1, 1, 2, 0, 0, 3, 1, 3],
        [1, 2, 2, 1, 1, 1, 2, 1],
        [1, 0, 2, 3, 3, 0, 1, 1],
        [2, 2, 2, 2, 2, 3, 3, 3]
    ],

    start: [0, 0],
    end: [7, 7],

    path: [],

    width: 25,
    height: 25,

    timeout: 200,

    current: null,

    equals: function(position1, position2) {
        return position1 && position2 && position1[0] == position2[0] && position1[1] == position2[1];
    },

    reset: function() {
        this.current = null;
        this.path = [];
        $('div[id^=maze-]').removeClass('active').removeClass('blocked');
    },

    draw: function() {
        var maze = $('#maze');
        maze.css({
            width: (this.data[0].length * this.width + 1) + 'px',
            height: (this.data.length * this.height + 1) + 'px'
        });
        $(this.data).each(function(i, line){
            $(line).each(function(j, value){
                var cell = $('<div>').attr({
                    id: 'maze-' + i + '-' + j,
                    class: 'maze' + value
                }).css({
                    position: 'absolute',
                    width: Maze.width + 'px',
                    height: Maze.height + 'px',
                    left: (j * Maze.width) + 'px',
                    top: (i * Maze.height) + 'px'
                });
                if (i == 0) {
                    cell.addClass('top');
                }
                if (j == 0) {
                    cell.addClass('left');
                }
                maze.append(cell);
            });
        });
        this.cellDiv(this.start).addClass('start');
        this.cellDiv(this.end).addClass('end');
    },

    tryTop: function() {
        var x = this.current[0];
        var y = this.current[1];
        if (x > 0) {
            var neighbor = this.data[x - 1][y];
            if (neighbor == 0 || neighbor == 1) {
                return [x - 1, y];
            }
        }
        return false;
    },

    tryRight: function() {
        var x = this.current[0];
        var y = this.current[1];
        if (y < this.data[0].length - 1) {
            var value = this.data[x][y];
            if (value == 0 || value == 2) {
                return [x, y + 1];
            }
        }
        return false;
    },

    tryBottom: function() {
        var x = this.current[0];
        var y = this.current[1];
        if (x < this.data.length - 1) {
            var value = this.data[x][y];
            if (value == 0 || value == 1) {
                return [x + 1, y];
            }
        }
        return false;
    },

    tryLeft: function() {
        var x = this.current[0];
        var y = this.current[1];
        if (y > 0) {
            var neighbor = this.data[x][y - 1];
            if (neighbor == 0 || neighbor == 2) {
                return [x, y - 1];
            }
        }
        return false;
    },

    cellDiv: function(position) {
        return $('#maze-' + position[0] + '-' + position[1]);
    },

    isActiveOrBlocked: function(position) {
        var div = this.cellDiv(position);
        return div.hasClass('active') || div.hasClass('blocked');
    },

    nextCell: function() {
        if (this.current == null) {
            return this.start;
        }
        var top = this.tryTop();
        var right = this.tryRight();
        var bottom = this.tryBottom();
        var left = this.tryLeft();
        var next = false;
        if (top && !this.isActiveOrBlocked(top)) {
            next = top;
        }
        else if (right && !this.isActiveOrBlocked(right)) {
            next = right;
        }
        else if (bottom && !this.isActiveOrBlocked(bottom)) {
            next = bottom;
        }
        else if (left && !this.isActiveOrBlocked(left)) {
            next = left;
        }
        return next;
    },

    step: function() {
        var next = this.nextCell();
        if (next) {
            this.cellDiv(next).addClass('active');
            this.current = next;
            this.path.push(this.current);
        }
        else {
            this.cellDiv(this.current).removeClass('active').addClass('blocked');
            this.path.pop();
            this.current = this.path[this.path.length - 1];

        }
        if (this.equals(next, this.end)) {
            $('#toolbar').show();
            return;
        }
        setTimeout('Maze.step()', this.timeout);
    }

};

$().ready(function(){
    Maze.draw();
    Maze.reset();
    Maze.step();
    $('#try-again').click(function(){
        $('#toolbar').hide();
        Maze.reset();
        Maze.step();
    });
});